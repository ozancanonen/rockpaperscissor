﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMover : MonoBehaviour
{
    //the list of tiles platforms, we add the prefabs from inspector
        public List<GameObject> TileList = new List<GameObject>();
        private List<GameObject> inGameTileList = new List<GameObject>();
        private int currentTile;

        public float shiftSpeed;
        public float spawnRate;

        public Vector3 defaultSpawnPos;
        public bool spawnImmediate;
        public Vector3 immediateSpawnPos;
        float spawnTimer;

    void Awake()
    {
        Spawn();
        SpawnImmediate();

    }
        void Update()
    {
        //if (game.GameOver)
        //{
        //    return;
        //}
        Shift();
        spawnTimer += Time.deltaTime;
        if(spawnTimer > spawnRate)
        {
            Spawn();
            spawnTimer = 0;
        }
    }

    void Spawn()
    {
        GameObject newTile = Instantiate(TileList[Random.Range(0,TileList.Count)], defaultSpawnPos, Quaternion.identity);
        inGameTileList.Add(newTile);
        newTile.transform.SetParent(gameObject.transform);
        newTile.name = "newTile" + currentTile;
        currentTile++;
    }

    void SpawnImmediate()
    {
        GameObject newTile = Instantiate(TileList[Random.Range(0, TileList.Count)], immediateSpawnPos, Quaternion.identity);
        newTile.transform.SetParent(gameObject.transform);
        inGameTileList.Add(newTile);
        newTile.name = "newTile" + currentTile;
    }

    void Shift()
    {
        for (int i=0; i<inGameTileList.Count; i++) {
            if (inGameTileList[i] != null)
            {
                inGameTileList[i].transform.localPosition += -Vector3.right * shiftSpeed * Time.deltaTime;
            }
        }
    }
}

