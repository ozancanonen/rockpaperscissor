﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ally : MonoBehaviour
{
    private GameManager gm;
    public GameObject Player;
    int number;
    // Start is called before the first frame update
    void Start()
    {
        gm = GetComponentInParent<GameManager>();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.transform.tag == "Player")
        {
            gm.spawnX= gm.spawnX + 0.4f;
            var newPlayer = Instantiate(Player, col.gameObject.transform.position + new Vector3(-gm.spawnX, +1f, 0f), Quaternion.identity);
            gm.jumpDelay = gm.jumpDelay + 0.02f;
            //number = Random.Range(0, 1);
            //if (number == 0)
            //{
            //    newPlayer.GetComponent<CapsuleCollider2D>().offset = new Vector2(0f, -0.1f);
            //}
            gm.HordeList.Add(newPlayer);
            newPlayer.transform.SetParent(gm.transform.GetChild(1));
            gm.addAllyScore();
            Destroy(gameObject);
        }
    }

}
