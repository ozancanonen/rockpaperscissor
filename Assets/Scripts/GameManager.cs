﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    public TextMeshProUGUI allyScoreText;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI coinScoreText;
    public TextMeshProUGUI highscoreText;
    private int coinScore;
    [HideInInspector] public int score;
    [HideInInspector] public float spawnX = 0.2f;
    [HideInInspector] public float jumpDelay;
    GameObject kingPlayer;
    int number;
    Vector3 allySpawnPos;
    [HideInInspector] public List<GameObject> HordeList;
    [HideInInspector] public bool playerIsJumping;
    [HideInInspector] public int allyScore = 1;
    [HideInInspector] public int allyScoreCollider;
    [HideInInspector] public float jumpTimeCounter;
    [HideInInspector] public float jumpTimeCounterHorde;
    [HideInInspector] public bool isJumping;

    private void Start()
    {
        HordeList = new List<GameObject>(GameObject.FindGameObjectsWithTag("Player"));
        highscoreText.text ="Highscore:"+ PlayerPrefs.GetInt("HighScore");  
    }

    public void setHighscore()
    {
        if (score > PlayerPrefs.GetInt("HighScore"))
        {
            PlayerPrefs.SetInt("HighScore", score);
            highscoreText.text = "Highscore:" + score;
        }

    }
    public void addAllyScore()
    {
        allyScore++;
        allyScoreText.text = "Ally:" + allyScore;
    }
    public void decreaseAllyScore()
    {
        allyScore--;
        allyScoreText.text = "Ally:" + allyScore;
    }
    public void addScore()
    {
        score++;
        scoreText.text = "Score:" + score;
        setHighscore();
    }
    public void addCoinScore()
    {
        coinScore++;
        coinScoreText.text = ":" + coinScore;
    }
    public void countTime()
    {
        jumpTimeCounter -= Time.deltaTime;
        countTimeHorde();
    }
    public void countTimeHorde()
    {
        jumpTimeCounterHorde -= Time.deltaTime;
    }

    public void creatingNewKingPlayer()
    {
        float bestX=-8;
        
        HordeList= new List<GameObject>(GameObject.FindGameObjectsWithTag("Player"));
        foreach (GameObject playerFromHorde in HordeList)
        {
            if (playerFromHorde.name!="Player")
            {
                if (playerFromHorde.transform.position.x > bestX)
                {
                    bestX = playerFromHorde.transform.position.x;
                    kingPlayer = playerFromHorde;
                }
            }
        }
        if (kingPlayer != null)
        {
            kingPlayer.name = "Player";
        }
    }
}
    

