﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    //public static PlayerController instance;

    private Rigidbody2D rb;
    private bool isGrounded;
    private bool hordeIsGrounded;
    private bool hordeJumped=false;
    public Transform feetPos;
    public float checkRadius;
    public Vector2 checkBoxRadius;
    public float jumpForce;
    public LayerMask whatIsGround;

    private float jumpTimeCounterforOtherPlayers;
    public float jumpTime;
    private bool collidedCounterRace=false;
    private bool mixTheHordeExceptKingBool = false;
    private GameManager gm;
    private float startTime;
    private float startTimeForMix;
    private float journeyLength;
    bool postControllerCheck;
    bool kingPostControllerCheck;
    private bool buttonUp=true;
    private float speed = 0.03f;


    void Start()
    {
        //instance = this;
        rb = GetComponent<Rigidbody2D>();
        gm = GetComponentInParent<GameManager>();
        gm.jumpTimeCounterHorde = jumpTime;
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.transform.tag == "Enemy")
        {
            gm.decreaseAllyScore();
            if (gm.allyScore == 0)
            {
                Destroy(gameObject);
                //int savedScore = PlayerPrefs.GetInt("Highscore");
                //if (score > savedScore)
                //{
                //    PlayerPrefs.SetInt("Highscore", score);
                //}
                SceneManager.LoadScene("Lose Screen");
            }
            else
            {
                if (gameObject.name == "Player")
                {
                    gm.creatingNewKingPlayer();
                }
                Destroy(gameObject);
                Destroy(col.gameObject);
               
            }
        }
        if (col.transform.tag == "CounterRace"&& !collidedCounterRace)
        {
            gm.allyScoreCollider++;
            collidedCounterRace = true;
            if (gm.allyScoreCollider >= 3)
            {
                gm.allyScoreCollider = 0;
                Destroy(col.gameObject);
                startTimeForMix = Time.time;
                mixTheHordeExceptKingBool = true;
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.transform.tag == "Coin")
        {
            gm.addCoinScore();
            Destroy(col.gameObject);
        }
        if (col.transform.tag == "ScoreZone")
        {
            gm.allyScoreCollider = 0;
            collidedCounterRace = false;
            mixTheHordeExceptKingBool = false;
            gm.addScore();
        }

        if (gameObject.name != "Player" && col.gameObject.tag == "PosController" && -10 < gameObject.transform.position.x && 
            gameObject.transform.position.x < -8)
        {
                postControllerCheck = true;
                startTime = Time.time;
                journeyLength = Vector3.Distance(transform.position, transform.position + new Vector3(2f, 0f, 0f));            
        }
        else
        {
            postControllerCheck = false;
        }
        if (gameObject.name == "Player" && col.gameObject.tag == "PosController" && -9 < gameObject.transform.position.x &&
            gameObject.transform.position.x < -6)
        {
            kingPostControllerCheck = true;
            startTime = Time.time;
            journeyLength = Vector3.Distance(transform.position, transform.position + new Vector3(3f, 0f, 0f));
        }
        else
        {
            kingPostControllerCheck = false;
        }

    }
    void FixedUpdate()
    {
        if (mixTheHordeExceptKingBool)
        {
            float distanceCovered = (Time.time - startTimeForMix) * 1f;
            Vector3 mixRange= new Vector3(Random.Range(0.1f, 0.5f), 0f, 0f);
            float fractionJourney = distanceCovered / mixRange.x;
            if (gameObject.name == "Player")
            {
                gameObject.transform.position = Vector3.Lerp(transform.position, transform.position + mixRange, fractionJourney * speed);
            }
            else
            {
                gameObject.transform.position = Vector3.Lerp(transform.position, transform.position - mixRange, fractionJourney * speed);
            }
        }
    
        if (kingPostControllerCheck)
        {
            float distCovered = (Time.time - startTime) * 1f;
            float fracJourney = distCovered / journeyLength;
            gameObject.transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(3f, 0f, 0f), fracJourney * speed);

        }
        if (postControllerCheck)
        {
            float distCovered = (Time.time - startTime) * 1f;
            float fracJourney = distCovered / journeyLength;
            gameObject.transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(2f, 0f, 0f), fracJourney*speed);     
        }
        
        if (gameObject.name == "Player")
        {
            isGrounded = Physics2D.OverlapBox(feetPos.position, checkBoxRadius,0f, whatIsGround);
            if (isGrounded)
            {
                gameObject.GetComponent<Animator>().SetBool("isJumping", false);
                gm.playerIsJumping = false;
            }
            if (isGrounded && Input.GetMouseButton(0)&&buttonUp)
            {
                buttonUp = false;
                gm.isJumping = true;
                gm.playerIsJumping = true;
                gameObject.GetComponent<Animator>().SetBool("isJumping", true);
                gm.jumpTimeCounter = jumpTime;
                //Debug.Log("birinci zıplama verildi");
                rb.velocity = Vector2.up * jumpForce;
            }
            if (Input.GetMouseButton(0) && gm.isJumping&&!buttonUp)
            {
                gameObject.GetComponent<Animator>().SetBool("isJumping", true);
                if (gm.jumpTimeCounter > 0)
                {
                    gm.playerIsJumping = true;
                    rb.velocity = Vector2.up * jumpForce;
                    gm.countTime();
                }
                else
                {
                    gm.isJumping = false;
                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                //Debug.Log("mouse basma kaldırıldı");
                gm.isJumping = false;
                buttonUp = true;
            }

        }
        else
        {
            hordeIsGrounded = Physics2D.OverlapBox(feetPos.position, checkBoxRadius, 0f, whatIsGround);
            if (hordeIsGrounded)
            {
                gameObject.GetComponent<Animator>().SetBool("isJumping", false);
            }
            if (hordeIsGrounded &&gm.playerIsJumping&& gm.jumpTimeCounterHorde < jumpTime- gm.jumpDelay)
            { 
                gameObject.GetComponent<Animator>().SetBool("isJumping", true);
                rb.velocity = Vector2.up * jumpForce;
                hordeJumped = true;
            }
            if (Input.GetMouseButton(0) && !hordeIsGrounded&& hordeJumped)
            {
                if (gm.jumpTimeCounterHorde > -gm.jumpDelay && (gm.jumpTimeCounterHorde < jumpTime -gm.jumpDelay) && gm.playerIsJumping)
                {
                    gameObject.GetComponent<Animator>().SetBool("isJumping", true);
                    rb.velocity = Vector2.up * jumpForce;
                    if (!gm.isJumping)
                    {
                        gm.countTimeHorde();
                    }
                }
                else
                {
                    gm.jumpTimeCounterHorde = jumpTime;
                    hordeJumped = false;
                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                gm.jumpTimeCounterHorde = jumpTime;
                hordeJumped = false;
            }
        }
    }   
}
    

